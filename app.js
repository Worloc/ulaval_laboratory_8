/**
 * Created by tourte_a on 06/11/14.
 */
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var tasks = require('./routes/tasks');

var app = express();

var corsOptions = {
    origin: '*',
    methods: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE', 'UPDATE'],
    credentials: true
};

app.use(cors(corsOptions));
app.use(bodyParser.json());


// Map des urls disponibles vers leurs fonctions correspondantes

app.get('/tasks', tasks.getTasks);
app.get('/tasks/:id', tasks.getTask);

app.post('/tasks/:id', tasks.createTask);

app.put('/tasks/:id', tasks.updateTask);

app.delete('/tasks/:id', tasks.deleteTask);

app.listen(5000);