/**
 * Created by tourte_a on 06/11/14.
 */
var tasks = [];

exports.getTasks = function(req, res) {
    res.status(200).send({"tasks": tasks});
};

exports.createTask = function(req, res) {
    if (typeof req.body != "undefined" && typeof req.body.id != "undefined" && typeof req.body.task != "undefined") {
        for (var i = 0; i < tasks.length; ++i) {
            if (tasks[i].id == req.body.id) {
                res.status(400).send({"error":"Bad Request"});
                return;
            }
        }
        tasks.push(req.body);
        res.status(201).send({"tasks": tasks});
    } else
        res.status(400).send({"error":"Bad Request"});
};

exports.getTask = function(req, res) {
    var task = {};
    var id = req.param("id");

    for (var i = 0; i < tasks.length; ++i) {
        if (tasks[i].id == id) {
            task = tasks[i];
            break;
        }
    }

    if (task.id == id)
        res.status(200).send({"task": task});
    else
        res.status(404).send({"error":"Not Found"});
}

exports.updateTask = function(req, res) {
    var task = req.body;
    var id = req.param("id");

    for (var i = 0; i < tasks.length; ++i) {
        if (tasks[i].id == id) {

            if (typeof req.body != "undefined" && typeof req.body.id != "undefined" && typeof req.body.task != "undefined") {
                tasks[i] = task;
                res.status(201).send({"tasks": tasks});
            } else
                res.status(400).send({"error": "Bad Request"});
            return;
        }
    }
    res.status(404).send({"error":"Not Found"});
}

exports.deleteTask = function(req, res) {
    var id = req.param("id");

    for (var i = 0; i < tasks.length; ++i) {
        if (tasks[i].id == id) {
            tasks.splice(i, 1);
            res.status(200).send({"tasks":tasks});
            return;
        }
    }
    res.status(404).send({"error":"Not Found"});
}